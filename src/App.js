import Calendar from "./components/calendar";
import './App.css';

function App() {
  return (
    <div >
      <Calendar/>
    </div>
  );
}

export default App;
